package com.thcode.api.customers.business.validators;

import com.thc.code.customers.CustomerDTO;
import com.thcode.api.customers.domain.Customer;
import com.thcode.api.customers.exceptions.CustomerNotFoundException;
import com.thcode.api.customers.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Component
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class CustomerBusinessValidator {

    private final CustomerRepository customerRepository;

    public void validateCustomerExists(final Customer customer) {
        final Optional<Customer> customerOptional;

        if (!StringUtils.isEmpty(customer.getUid())) {
            customerOptional = customerRepository.findById(customer.getUid());
        } else {
            customerOptional = customerRepository.findByNameAndLastNameAndGovernmentIdentification(customer.getName(),
                    customer.getLastName(), customer.getGovernmentIdentification());
        }

        if (customerOptional.isEmpty()) {
            throw new CustomerNotFoundException(customer.getUid());
        }
    }

    public boolean validateCustomerExists(final String customerId) {
        validateCustomerExists(Customer.builder()
                .uid(customerId)
                .build());

        return true;
    }

    public boolean validateUpdate(CustomerDTO customer, final String customerId) {
        return validateCustomerExists(customerId)
                && org.apache.commons.lang3.StringUtils.isEmpty(customer.getUid());
    }
}