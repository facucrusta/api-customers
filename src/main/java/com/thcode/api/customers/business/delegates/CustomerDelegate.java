package com.thcode.api.customers.business.delegates;

import com.thc.code.customers.CustomerDTO;
import com.thcode.api.customers.business.validators.CustomerBusinessValidator;
import com.thcode.api.customers.domain.Customer;
import com.thcode.api.customers.exceptions.BadRequestException;
import com.thcode.api.customers.exceptions.BusinessRuleErrorException;
import com.thcode.api.customers.exceptions.CustomerAlreadyExistsException;
import com.thcode.api.customers.exceptions.CustomerNotFoundException;
import com.thcode.api.customers.repositories.CustomerRepository;
import com.thcode.api.customers.transformers.fromdomain.CustomerTransformer;
import com.thcode.api.customers.transformers.todomain.CustomerDtoTransformer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Optional;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CustomerDelegate {

    private final CustomerRepository customerRepository;
    private final CustomerBusinessValidator customerBusinessValidator;
    private final CustomerDtoTransformer customerDtoTransformer;
    private final CustomerTransformer customerTransformer;

    public CustomerDTO save(final CustomerDTO customerDTO, String companyId) {
        final Customer customer = customerDtoTransformer.transform(customerDTO);
        final CustomerDTO savedCustomer;

        customer.setCompanyId(companyId);
        customer.setCreationDate(LocalDate.now());

        try {
            savedCustomer = customerTransformer.transform(customerRepository.save(customer));
        } catch (DataIntegrityViolationException e) {
            throw new CustomerAlreadyExistsException(customerDTO.getUid());
        }

        return savedCustomer;
    }

    public CustomerDTO get(final String customerId, String companyId) {
        if (StringUtils.isEmpty(customerId)) {
            throw new BadRequestException("Missing customerId on request");
        }

        final Optional<Customer> customer = customerRepository.findByUidAndCompanyId(customerId, companyId);

        return customer
                .map(customerTransformer::transform)
                .orElseThrow(() -> new CustomerNotFoundException(customerId));
    }

    public void delete(final String customerId, final String companyId) {
        if (customerBusinessValidator.validateCustomerExists(customerId)) {
            customerRepository.deleteById(customerId);
        } else {
            throw new CustomerNotFoundException(customerId);
        }
    }

    public CustomerDTO update(final CustomerDTO customer, String customerId, String companyId) {
        if (customerBusinessValidator.validateUpdate(customer, customerId)) {
            customer.setUid(customerId);

            final Optional<Customer> optionalCustomer = customerRepository.findByUidAndCompanyId(customerId, companyId);

            if (optionalCustomer.isEmpty()) {
                throw new CustomerNotFoundException(customerId);
            }

            final Customer currentCustomer = optionalCustomer.get();

            return customerTransformer
                    .transform(customerRepository
                            .save(currentCustomer.updateInformation(customer)));
        }

        throw new BusinessRuleErrorException("not allowed to update companyId value");
    }

}
