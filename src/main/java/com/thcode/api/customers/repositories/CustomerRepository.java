package com.thcode.api.customers.repositories;

import com.thcode.api.customers.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, String> {

    Optional<Customer> findByUidAndCompanyId(final String uid, final String companyId);
    Optional<Customer> findByNameAndLastNameAndGovernmentIdentification(final String name, final String lastName, final String dni);
}
