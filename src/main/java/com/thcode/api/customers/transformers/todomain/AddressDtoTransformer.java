package com.thcode.api.customers.transformers.todomain;

import com.thc.code.customers.AddressDTO;
import com.thcode.api.customers.domain.Address;
import org.apache.commons.collections4.Transformer;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class AddressDtoTransformer implements Transformer<AddressDTO, Address> {
    
    @Override
    public Address transform(AddressDTO addressDTO) {
        if (isNull(addressDTO)) {
            return null;
        }

        return Address.builder()
                .city(addressDTO.getCity())
                .department(addressDTO.getDepartment())
                .floor(addressDTO.getFloor())
                .number(addressDTO.getNumber())
                .province(addressDTO.getProvince())
                .street(addressDTO.getStreet())
                .uid(addressDTO.getUid())
                .zipCode(addressDTO.getZipCode())
                .build();
    }
}
