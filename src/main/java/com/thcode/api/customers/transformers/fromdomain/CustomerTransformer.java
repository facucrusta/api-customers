package com.thcode.api.customers.transformers.fromdomain;

import com.thc.code.customers.AddressDTO;
import com.thc.code.customers.CustomerDTO;
import com.thcode.api.customers.domain.Customer;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class CustomerTransformer implements Transformer<Customer, CustomerDTO> {

    private final AddressTransformer addressTransformer;

    @Override
    public CustomerDTO transform(Customer customer) {
        if (isNull(customer)) {
            return null;
        }

        return CustomerDTO.builder()
                .cellPhone(customer.getCellPhone())
                .address(addressTransformer.transform(customer.getAddress()))
                .companyId(customer.getCompanyId())
                .creationDate(customer.getCreationDate())
                .deletionDate(customer.getDeletionDate())
                .email(customer.getEmail())
                .gender(customer.getGender())
                .governmentIdentification(customer.getGovernmentIdentification())
                .governmentIdentificationType(customer.getGovernmentIdentificationType())
                .lastName(customer.getLastName())
                .middleName(customer.getMiddleName())
                .name(customer.getName())
                .suffix(customer.getSuffix())
                .telephoneNumber(customer.getTelephoneNumber())
                .tributeKey(customer.getTributeKey())
                .tributeKeyType(customer.getTributeKeyType())
                .uid(customer.getUid())
                .build();
    }
}
