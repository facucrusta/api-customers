package com.thcode.api.customers.transformers.todomain;

import com.thc.code.customers.CustomerDTO;
import com.thcode.api.customers.domain.Customer;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class CustomerDtoTransformer implements Transformer<CustomerDTO, Customer> {

    private final AddressDtoTransformer addressDtoTransformer;

    @Override
    public Customer transform(CustomerDTO customerDTO) {
        if (isNull(customerDTO)) {
            return null;
        }

        return Customer.builder()
                .name(customerDTO.getName())
                .middleName(customerDTO.getMiddleName())
                .lastName(customerDTO.getLastName())
                .suffix(customerDTO.getSuffix())
                .email(customerDTO.getEmail())
                .cellPhone(customerDTO.getCellPhone())
                .companyId(customerDTO.getCompanyId())
                .creationDate(customerDTO.getCreationDate())
                .deletionDate(customerDTO.getDeletionDate())
                .gender(customerDTO.getGender())
                .uid(customerDTO.getUid())
                .governmentIdentification(customerDTO.getGovernmentIdentification())
                .governmentIdentificationType(customerDTO.getGovernmentIdentificationType())
                .telephoneNumber(customerDTO.getTelephoneNumber())
                .tributeKey(customerDTO.getTributeKey())
                .tributeKeyType(customerDTO.getTributeKeyType())
                .address(addressDtoTransformer.transform(customerDTO.getAddress()))
                .build();
    }
}
