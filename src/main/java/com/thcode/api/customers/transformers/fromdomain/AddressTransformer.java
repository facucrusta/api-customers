package com.thcode.api.customers.transformers.fromdomain;

import com.thc.code.customers.AddressDTO;
import com.thcode.api.customers.domain.Address;
import org.apache.commons.collections4.Transformer;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class AddressTransformer implements Transformer<Address, AddressDTO> {

    @Override
    public AddressDTO transform(Address address) {
        if (isNull(address)) {
            return null;
        }

        return AddressDTO.builder()
                .city(address.getCity())
                .department(address.getDepartment())
                .floor(address.getFloor())
                .number(address.getNumber())
                .province(address.getProvince())
                .street(address.getStreet())
                .uid(address.getUid())
                .zipCode(address.getZipCode())
                .build();
    }
}
