package com.thcode.api.customers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ApiCustomersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCustomersApplication.class, args);
	}

}
