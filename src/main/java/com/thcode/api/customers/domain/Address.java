package com.thcode.api.customers.domain;

import com.thc.code.customers.AddressDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Optional;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "ADDRESS")
public class Address {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "UID", nullable = false, unique = true)
    private String uid;

    @Column(name = "STREET")
    private String street;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "FLOOR")
    private String floor;

    @Column(name = "DEPARTMENT")
    private String department;

    @Column(name = "ZIP_CODE")
    private String zipCode;

    @Column(name = "CITY")
    private String city;

    @Column(name = "PROVINCE")
    private String province;

    public Address updateInformation(AddressDTO address) {
        Optional.ofNullable(address).map(AddressDTO::getStreet).map(this::setStreet);
        Optional.ofNullable(address).map(AddressDTO::getNumber).map(this::setNumber);
        Optional.ofNullable(address).map(AddressDTO::getFloor).map(this::setFloor);
        Optional.ofNullable(address).map(AddressDTO::getDepartment).map(this::setDepartment);
        Optional.ofNullable(address).map(AddressDTO::getZipCode).map(this::setZipCode);
        Optional.ofNullable(address).map(AddressDTO::getCity).map(this::setCity);
        Optional.ofNullable(address).map(AddressDTO::getProvince).map(this::setProvince);
        return this;
    }

}