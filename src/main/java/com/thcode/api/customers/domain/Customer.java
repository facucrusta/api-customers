package com.thcode.api.customers.domain;

import com.thc.code.customers.CustomerDTO;
import com.thc.code.customers.Gender;
import com.thc.code.customers.GovernmentIdentificationType;
import com.thc.code.customers.TributeKeyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Optional;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "CUSTOMER", uniqueConstraints = { @UniqueConstraint( columnNames = { "COMPANY_ID", "GOVERNMENT_IDENTIFICATION", "TRIBUTE_KEY" } ) } )
public class Customer {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "UID", nullable = false, unique = true)
    private String uid;

    @Column(name = "COMPANY_ID")
    private String companyId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "SUFFIX")
    private String suffix;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "CELL_PHONE")
    private String cellPhone;

    @Enumerated(EnumType.STRING)
    @Column(name = "GENDER")
    private Gender gender;

    @Column(name = "TELEPHONE_NUMBER")
    private String telephoneNumber;

    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "UID")
    private Address address;

    @Column(name = "CREATION_DATE", columnDefinition = "DATE")
    private LocalDate creationDate;

    @Column(name = "DELETION_DATE", columnDefinition = "DATE")
    private LocalDate deletionDate;

    @Column(name = "TRIBUTE_KEY")
    private String tributeKey;

    @Enumerated(EnumType.STRING)
    @Column(name = "TRIBUTE_KEY_TYPE")
    private TributeKeyType tributeKeyType;

    @Column(name = "GOVERNMENT_IDENTIFICATION")
    private String governmentIdentification;

    @Enumerated(EnumType.STRING)
    @Column(name = "GOVERNMENT_IDENTIFICATION_TYPE")
    private GovernmentIdentificationType governmentIdentificationType;

    public Customer updateInformation(CustomerDTO customer) {
        Optional.ofNullable(customer).map(CustomerDTO::getName).map(this::setName);
        Optional.ofNullable(customer).map(CustomerDTO::getMiddleName).map(this::setMiddleName);
        Optional.ofNullable(customer).map(CustomerDTO::getLastName).map(this::setLastName);
        Optional.ofNullable(customer).map(CustomerDTO::getSuffix).map(this::setSuffix);
        Optional.ofNullable(customer).map(CustomerDTO::getEmail).map(this::setEmail);
        Optional.ofNullable(customer).map(CustomerDTO::getGender).map(this::setGender);
        Optional.ofNullable(customer).map(CustomerDTO::getTelephoneNumber).map(this::setTelephoneNumber);
        Optional.ofNullable(customer).map(CustomerDTO::getTributeKey).map(this::setTributeKey);
        Optional.ofNullable(customer).map(CustomerDTO::getTributeKeyType).map(this::setTributeKeyType);
        Optional.ofNullable(customer).map(CustomerDTO::getGovernmentIdentification)
                .map(this::setGovernmentIdentification);
        Optional.ofNullable(customer).map(CustomerDTO::getGovernmentIdentificationType)
                .map(this::setGovernmentIdentificationType);
        return this;
    }
}

