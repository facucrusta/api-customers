package com.thcode.api.customers.resources;

import com.thc.code.constants.Headers;
import com.thc.code.customers.CustomerDTO;
import com.thcode.api.customers.business.delegates.CustomerDelegate;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping(value = "b2c/v1/customers")
public class CustomerResource {

    private final CustomerDelegate customerDelegate;

    @ApiOperation(
            value = "Get a customer by uid",
            consumes = "application/json",
            produces = "application/json"
    )
    @GetMapping(value = "/{customer-id}")
    public ResponseEntity<CustomerDTO> getCustomer(@PathVariable(value = "customer-id") final String customerId,
                                                   @RequestHeader(value = Headers.COMPANY_ID_HEADER) final String companyId) {
        return new ResponseEntity<>(customerDelegate.get(customerId, companyId), HttpStatus.OK);
    }

    @ApiOperation(
            value = "Register a new customer on the database",
            consumes = "application/json",
            produces = "application/json"
    )
    @PostMapping
    public ResponseEntity<CustomerDTO> saveCustomer(@RequestBody CustomerDTO customer,
                                                    @RequestHeader(value = Headers.COMPANY_ID_HEADER) final String companyId) {
        return new ResponseEntity<>(customerDelegate.save(customer, companyId), HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Delete a customer from the database",
            consumes = "application/json",
            produces = "application/json"
    )
    @DeleteMapping(value = "/{customer-id}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable(value = "customer-id") final String customerId,
                                               @RequestHeader(value = Headers.COMPANY_ID_HEADER) final String companyId) {
        customerDelegate.delete(customerId, companyId);

        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @ApiOperation(
            value = "Update customer data",
            consumes = "application/json",
            produces = "application/json"
    )
    @PatchMapping(value = "/{customer-id}")
    public ResponseEntity<CustomerDTO> updateCustomer(@RequestBody CustomerDTO customer,
                                                      @RequestHeader(value = Headers.COMPANY_ID_HEADER) final String companyId,
                                                      @PathVariable(value = "customer-id") final String customerId) {
        return new ResponseEntity<>(customerDelegate.update(customer, customerId, companyId), HttpStatus.OK);
    }
}