package com.thcode.api.customers.exceptions;

public class BusinessRuleErrorException extends RuntimeException {
    public BusinessRuleErrorException(String s) {
        super(s);
    }
}
