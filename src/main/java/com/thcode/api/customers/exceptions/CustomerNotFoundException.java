package com.thcode.api.customers.exceptions;

public class CustomerNotFoundException extends RuntimeException {
    public CustomerNotFoundException(final String customerId) {
        super(customerId);
    }
}
