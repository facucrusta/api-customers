package com.thcode.api.customers.exceptions.handlers;

import com.thc.code.errors.ErrorResponse;
import com.thcode.api.customers.exceptions.CustomerNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.google.common.collect.Lists.newArrayList;

@ControllerAdvice
public class CustomerNotFoundExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {CustomerNotFoundException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorResponse("THC-001", "Customer not found.", newArrayList(ex.getMessage())),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
