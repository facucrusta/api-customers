package com.thcode.api.customers.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomerAlreadyExistsException extends RuntimeException {
    public CustomerAlreadyExistsException(String uid) {
        super(uid);
    }
}
