package com.thcode.api.customers.exceptions.handlers;

import com.thc.code.errors.ErrorResponse;
import com.thcode.api.customers.exceptions.CustomerAlreadyExistsException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.google.common.collect.Lists.newArrayList;

@ControllerAdvice
public class CustomerAlreadyExistsExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {CustomerAlreadyExistsException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorResponse("THC-002", "This Customer already exists.", newArrayList(ex.getMessage())),
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
}