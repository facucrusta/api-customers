package com.thcode.api.customers.exceptions.handlers;

import com.thc.code.errors.ErrorResponse;
import com.thcode.api.customers.exceptions.BusinessRuleErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class BusinessRuleErrorExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {BusinessRuleErrorException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        log.error(ex.getMessage());

        return handleExceptionInternal(ex, new ErrorResponse("THC-003", ex.getMessage(), null),
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
