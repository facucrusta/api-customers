# Docker file config
FROM openjdk:11
VOLUME /tmp
ADD target/api-customers-*.jar /tmp/api-customers.jar

# Ports
EXPOSE 8080

ENTRYPOINT exec java $JAVA_OPTS -jar /tmp/api-customers.jar